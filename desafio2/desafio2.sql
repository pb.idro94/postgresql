
/*
1. Crear una base de datos mediante PostgreSQL basandonos en los artistas más escuchados en Spotify del 2018, para eso, es necesario que usted cree las siguientes tablas con sus respectivos datos:
Artista
  nombre_artista
  fecha_de_nacimiento
  nacionalidad
Canción
  titulo_cancion
  artista
  album
  numero_del_track
Albúm
  titulo_album
  artista
  año
*/

CREATE DATABASE spotify;

\c spotify

CREATE TABLE artista ( nombre_artista varchar(255), fecha_de_nacimiento date, nacionalidad varchar(255));

CREATE TABLE cancion ( titulo_cancion varchar(255), artista varchar(255), album varchar(255), numero_del_track int);

CREATE TABLE album ( titulo_album varchar(255), artista varchar(255),  ano int);


/*

2. Ingrese los datos del archivo Artistas_populares_2018 a sus respectiva tabla y responda a las siguientes consultas:

Canciones que salieron el año 2018

Albums y la nacionalidad de su artista

Número de track, cancion, album, año de lanzamiento y artista donde las canciones deberán estar ordenadas por año de lanzamiento del albúm, album y artista correspondiente.
*/

COPY artista FROM '/Users/administrador/Desafio Latam/postgresql/desafio2/Artista.csv' delimiter ',' csv header encoding 'windows-1251';

COPY cancion FROM '/Users/administrador/Desafio Latam/postgresql/desafio2/Cancion.csv' delimiter ',' csv header encoding 'windows-1251';

COPY album FROM '/Users/administrador/Desafio Latam/postgresql/desafio2/Album.csv' delimiter ',' csv header encoding 'windows-1251';


select cancion.titulo_cancion,album.ano as ano_de_cancion from cancion left join album on cancion.album=album.titulo_album  where album.ano = 2018;

select album.*,artista.nacionalidad as nacionalidad_artista from album left  join artista on album.artista=artista.nombre_artista;

select cancion.numero_del_track, cancion.titulo_cancion, album.titulo_album, album.ano, artista.nombre_artista from album inner join cancion  on cancion.album=album.titulo_album inner join artista on artista.nombre_artista = album.artista order by album.ano asc, album.titulo_album asc, artista.nombre_artista asc;
