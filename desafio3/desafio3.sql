

CREATE DATABASE pbustamante;

\c pbustamante

CREATE TABLE artista ( nombre_artista varchar(255), fecha_de_nacimiento date, nacionalidad varchar(255));

CREATE TABLE cancion ( titulo_cancion varchar(255), artista varchar(255), album varchar(255), numero_del_track int);

CREATE TABLE album ( titulo_album varchar(255), artista varchar(255),  ano int);


COPY artista FROM 'Artista.csv' delimiter ',' csv header encoding 'windows-1251';

COPY cancion FROM 'Cancion.csv' delimiter ',' csv header encoding 'windows-1251';

COPY album FROM 'Album.csv' delimiter ',' csv header encoding 'windows-1251';

with 

artist_data as(
select
fecha_de_nacimiento
, nacionalidad
, artista.nombre_artista
, cancion.numero_del_track
, cancion.titulo_cancion
, album.titulo_album
, album.ano  
from album 
inner join cancion  on cancion.album=album.titulo_album 
inner join artista on artista.nombre_artista = album.artista 
order by album.ano asc, album.titulo_album asc, artista.nombre_artista asc)

select 
numero_del_track
, titulo_cancion
, nombre_artista
, fecha_de_nacimiento
, nacionalidad 
from 
artist_data 
where extract(year from fecha_de_nacimiento)>1992 
and numero_del_track = 4 
and nacionalidad like '%Estado%'
limit 1;

