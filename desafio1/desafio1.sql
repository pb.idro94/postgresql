/* 1. Cree la base de datos llamada Biblioteca  */
CREATE DATABASE Biblioteca;

/*  2. En esta, cree la tabla Libro, cuyas campos serán id_libro (clave primeria), nombre_libro, autor, genero */
CREATE TABLE Libros ( id_libro int, nombre_libro varchar(255), autor varchar(255), genero varchar(255), primary key (id_libro));

/* 3. Ingrese el libro llamado Sapo y Sepo, el resto de los datos debe inventarlos */
insert into libros values (1,'sapo y sepo','jorge nitales','erotismo');

/* 4. Ingrese el libro llamado La Metamorfosis, el resto de los datos debe inventarlos */
insert into libros values (2,'La metamorfosis','elcuca rachas','autoayuda');

/* 5. Cree la tabla Prestamo, que contenga los campos id_prestamo (clave_primaria), id_libro (clave
foránea referenciando a id_libro en Libro), nombre_persona, fecha_inicio y fecha_termino */
CREATE TABLE prestamo ( id_prestamo int, id_libro int, nombre_persona varchar(255), fecha_inicio date, fecha_termino date, primary key (id_prestamo), FOREIGN KEY (id_libro) REFERENCES libros(id_libro));

/* 6. Añada la columna prestado (booleano) a la tabla Libro */
ALTER TABLE libros ADD prestado bool;

/* 7. Ingresar el estado de prestamo de Sapo y Sepo */
update libros set prestado = true where id_libro=1;

/* 8. Ingresar el estado de prestamo de La Metamorfosis */

update libros set prestado = false where id_libro=2;

/* 9. Ingrese 5 prestamos asociados a Sapo y Sepo */
insert into prestamo values (1,1,'pablo escobar','2018-05-05','2018-05-05');
insert into prestamo values (2,1,'chapo guzman','2018-05-05','2018-05-05');
insert into prestamo values (3,1,'jeinserverg','2018-05-05','2018-05-05');
insert into prestamo values (4,1,'diego arias','2018-05-05','2018-05-05');
insert into prestamo values (5,1,'mali desafio','2018-05-05','2018-05-05');

/* 10. Ingrese 6 prestamos asociados a La Metamorfosis */
insert into prestamo values (6,2,'julio vergolio','2018-05-05','2018-05-05');
insert into prestamo values (7,2,'aquiles baeza','2018-05-05','2018-05-05');
insert into prestamo values (8,2,'nona me','2018-05-05','2018-05-05');
insert into prestamo values (9,2,'bernardita cumple','2018-05-05','2018-05-05');
insert into prestamo values (10,2,'noseq poner','2018-05-05','2018-05-05');
insert into prestamo values (11,2,'piniera kung liao','2018-05-05','2018-05-05');

/* 11. Cree un nuevo libro */
insert into libros values (3,'la muerte de piniera','negromat apaco','romance',true);

/* 12. Seleccione los libros y las personas quienes lo pidieron prestado (nombre_libro y nombre_persona) */
select libros.nombre_libro, prestamo.nombre_persona from libros inner join prestamo on libros.id_libro=prestamo.id_libro;

/* 13. Seleccione todas las columnas de la tabla Prestamo para los prestamos de Sapo y Sepo, ordenados decrecientemente por fecha_de_inicio */
select * from prestamo where prestamo.id_libro=1 order by fecha_inicio desc;
